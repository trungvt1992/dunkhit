﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shakespeare;

public class GamePlayController : SingletonMonobehaviour<GamePlayController>
{
    public static readonly float MinYBasketBall = 0.2f;
    public static readonly float MaxYBasketBall = 0.7f;
    public Camera CurrentCamera;
    public BallController Ball;
    public Basket[] BasketList = new Basket[2];
    public GameObject Floor;

    public float Duration = 3f;
    public UnityEngine.UI.Slider TimeSlider;
    [HideInInspector]
    public Basket currentBastket;

    private float currentTime;
    private int currentBasketIndex;
    private int currentScore = 0;

    public bool isGameStarted { get; private set; }

    // Use this for initialization
    void Start()
    {
        Floor.transform.position = CurrentCamera.ViewportToWorldPoint(new Vector3(0, 0));

        currentBasketIndex = 1;

        currentBastket = BasketList[currentBasketIndex % 2];
        currentBastket.transform.position = GetBasketPostion(currentBastket.Side);
    }

    public void StartGame()
    {
        isGameStarted = true;
    }




    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        TimeSlider.value = 1 - currentTime / Duration;
        if (currentTime >= Duration)
        {
            EndGame();
        }
    }
    void FixedUpdate()
    {

    }
    public void TriggerExit(Collider2D collider)
    {
        if (collider.tag == " basket")
        {
        }
        else
        {
         
        }
    }

    public void TriggerEnter(Collider2D collider)
    {
        if (collider.gameObject.tag == "basket")
        {
            var isBallFallingDown = Ball.rigid.velocity.y < 0.0f;//Vector3.Dot((collider.gameObject.transform.position - Ball.transform.position).normalized, Vector3.up) < 0;
            if (!isBallFallingDown)
                return;

            currentTime = 0f;
            currentScore++;
            FloatingText.CreateFloatingText(currentBastket.textPosHolder.position, "", currentScore.ToString(), Color.blue);

            SetActiveBasket(currentBastket.transform.position, false);
            currentBasketIndex++;
            currentBastket = BasketList[currentBasketIndex % 2];

            SetActiveBasket(GetBasketPostion(currentBastket.Side), true);
            Debug.Log("current score:" + currentScore);
        }


    }
    public void SetActiveBasket(Vector3 position, bool isActive)
    {
        currentBastket.gameObject.SetActive(isActive);
        currentBastket.gameObject.transform.position = position;
    }
    private Vector2 GetBasketPostion(int side)
    {
        var pos = CurrentCamera.ViewportToWorldPoint(new Vector3(side, Random.Range(MinYBasketBall, MaxYBasketBall), 0));
        return new Vector3(pos.x, pos.y, 0);
    }



    /// <summary>
    /// check the ball is out side of camera 
    /// </summary>
    /// <param name="position"></param>
    /// <returns> return opposite side [0 :out right side - 1 : out left side]</returns>
    public int isPositionOutOfCamera(Vector3 position)
    {
        Vector3 ViewportPoint = CurrentCamera.WorldToViewportPoint(position);
        if (ViewportPoint.x >= 1)
        {
            return 0;
        }
        if (ViewportPoint.x <= 0)
            return 1;
        return -1;
    }

    public void EndGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("StartScene");
    }



}
