﻿using UnityEngine;
using System.Collections;

namespace Shakespeare
{
	interface ISingletonMonobehaviour
	{
		void InitSingleton ();
	}

	abstract public class SingletonMonobehaviour<T> : MonoBehaviour, ISingletonMonobehaviour 
	where T:MonoBehaviour
	{
		protected static T instance;
		static bool initialised = false;
	
		public static T Instance
		{
			get
			{
				if (!initialised && instance == null)
				{
					instance = GameObject.FindObjectOfType (typeof(T)) as T;

					if (instance != null)
					{
						((ISingletonMonobehaviour)instance).InitSingleton ();
					}
				}

				initialised = true;
			
				return instance;
			}
		
			private set
			{
				instance = value;
			}
		}

		virtual public void InitSingleton ()
		{
		}
		
		virtual protected void OnDestroy()
		{
			if (instance == this)
			{
				instance = null;
				initialised = false;
			}
		}
	
		virtual protected void Awake ()
		{
			if (instance == null)
			{
				instance = this as T;
				((ISingletonMonobehaviour)instance).InitSingleton ();
			}
			else if (instance != this)
			{
				GameObject.Destroy(this.gameObject); //dont allow duplicates!
			}

		}
	
		/*public SingletonMonobehaviour()
		: base()
	{
		instance = this as T;
	}*/
	}
}