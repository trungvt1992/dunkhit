﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{

    public float ForceValue = 3000f;
    public float ForcexValue = 1000f;
    public float degree = 30f;

    [HideInInspector]
    public Rigidbody2D rigid;
    //private Vector2 MoveUpDirection;
    private Vector2 directionx;
    private Vector2 updirection;
    private CircleCollider2D circleCollider;

    // Use this for initialization
    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        directionx = Vector3.right;
        updirection = new Vector2(Mathf.Cos(degree * Mathf.Deg2Rad), Mathf.Sin(degree * Mathf.Deg2Rad));
        circleCollider = GetComponent<CircleCollider2D>();

    }
    public Vector2 MoveUpDirection(float Degree)
    {
        return new Vector2(Mathf.Cos(Degree * Mathf.Deg2Rad), Mathf.Sin(Degree * Mathf.Deg2Rad));
    }

    public void Init()
    {

    }

    float dt = 0f;
    float CheckOutCameraduration = 2f;
    // Update is called once per frame
    void Update()
    {
    
    }
    public void MoveBallToOpposideSide(float side)
    {
        Vector3 viewportPoint = GamePlayController.Instance.CurrentCamera.WorldToViewportPoint(transform.position);
        float forceValue = 1;
        if (rigid.velocity.y < 1 && side == GamePlayController.Instance.currentBastket.Side)
        {
            side = 1 - side;
            forceValue = 5000f;
        }
            
      
        Vector3 worldpoint = GamePlayController.Instance.CurrentCamera.ViewportToWorldPoint(new Vector3(side, viewportPoint.y, viewportPoint.z));
        var radius = rigid.velocity.x < 0 ? circleCollider.radius : -circleCollider.radius;
        rigid.position = new Vector2(worldpoint.x + radius, worldpoint.y);

        rigid.AddForce(-forceValue * rigid.velocity.normalized * Time.deltaTime, ForceMode2D.Impulse);

    }
    private void FixedUpdate()
    {

        if (Input.GetMouseButtonDown(0))
        {

            rigid.velocity = Vector2.zero;
            var currentDegree = GamePlayController.Instance.currentBastket.Side == 0 ? 180 - degree : degree;
            var dir = MoveUpDirection(currentDegree);
            rigid.AddForce(dir * Time.deltaTime * ForceValue, ForceMode2D.Impulse);
            rigid.AddTorque(GamePlayController.Instance.currentBastket.Side != 0 ? 180 - degree : degree);

        }
        else
        {

            dt += Time.deltaTime;
            var radius = rigid.transform.position.x < 0 ? circleCollider.radius : -circleCollider.radius;
            var side = GamePlayController.Instance.isPositionOutOfCamera(new Vector3(transform.position.x + radius, transform.position.y, transform.position.z));
            if (side == -1 || dt < CheckOutCameraduration)
                return;
            dt = 0;
            MoveBallToOpposideSide(side);
        }

    }

}
