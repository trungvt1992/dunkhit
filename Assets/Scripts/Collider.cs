﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Collider : MonoBehaviour {

    // Use this for initialization

    void OnCollisionEnter2D(Collision2D collider)
    {
        
        if (collider.gameObject.tag == "ball")
        {
            Debug.Log("OnCollisionEnter2D");
            GamePlayController.Instance.TriggerEnter(GetComponent<Collider2D>());
        }
    }
    public void OnCollisionExit2D(Collision2D collider)
    {
        // Destroy everything that leaves the trigger
        if (collider.gameObject.tag == "ball")
        {
            Debug.Log("OnCollisionExit2D");
            GamePlayController.Instance.TriggerExit(GetComponent<Collider2D>());
        }
    }
    void OnTriggerEnter2D(Collider2D collider)
    {

        if (collider.tag == "ball")
        {
           
            Debug.Log("OnColliderEnter2D" + (collider as CircleCollider2D).radius);
            GamePlayController.Instance.TriggerEnter(GetComponent<Collider2D>());
        }
    }
}
