﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Shakespeare;

public class StartGameController : SingletonMonobehaviour<StartGameController>
{

    // Use this for initialization
    public UnityEngine.UI.Button PlayButton;
	void Start () {
		
	}
    public void PlayGame()
    {
        SceneManager.LoadScene("gamescene");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
