﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using TMPro;

    public class FloatingText : MonoBehaviour
    {
        public enum TextEffect
        {
            Float = 0,
            Bounce
        }

        float mLifeTime = 0.4f;
        float mFadeTime = 0.2f;
        float mYMoveDistance = 3.0f;

        public void Init(string fontName, string content, Color color, TextEffect effect = FloatingText.TextEffect.Float)
        {
            switch (effect)
            {
                case TextEffect.Float:
                    mLifeTime = 2f;
                    transform.DOMoveY(transform.position.y + mYMoveDistance, mLifeTime + mFadeTime).OnComplete(OnDone);
                    break;
                case TextEffect.Bounce:
                    float power = 2.5f;
                    mLifeTime = 0.8f + 0.6f + 0.4f;
                    Vector3 random_pos = transform.position + new Vector3(Random.Range(-0.8f, 0.8f), 0, 0);
                    Sequence backwardsTween = DOTween.Sequence();
                    backwardsTween.Append(transform.DOJump(random_pos, power, 1, 0.3f));
                    backwardsTween.Append(transform.DOJump(random_pos, power / 2, 1, 0.2f));
                    backwardsTween.Append(transform.DOJump(random_pos, power / 4, 1, 0.1f).OnComplete(() => {
                        Destroy(gameObject, mFadeTime);
                    }));
                    break;
            }

            GameObject go = Instantiate<GameObject>(Resources.Load("pre_CarterOne_SDF") as GameObject) ;
            if (go != null)
            {
                TextMeshPro textMeshPro = go.GetComponent<TextMeshPro>();
                textMeshPro.color = color;
                textMeshPro.text = content;

                go.transform.SetParent(transform);
                go.transform.localPosition = Vector3.zero;
            }
            else
            {
                Debug.LogError("Empty resouce font pre_CarterOne_SDF ");
            }
        }

        void OnDone()
        {
            Destroy(gameObject);
        }


        //Static
        static public FloatingText CreateFloatingText(Vector3 beginPos, string fontName, string content, Color color, TextEffect effect = FloatingText.TextEffect.Float)
        {
            GameObject ret = null;
            ret = new GameObject("FloatingText");
            ret.transform.position = beginPos;
            FloatingText text = ret.AddComponent<FloatingText>();
            text.Init(fontName, content, color, effect);
            return text;
        }
    }

