﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasketCollider : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void OnTriggerEnter2D(Collider2D collider)
    {
        
        if (collider.tag == "ball")
        {
            Debug.Log("OnTriggerEnter2D");
            GamePlayController.Instance.TriggerEnter(collider);
        }
    }
}
