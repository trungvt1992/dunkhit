﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorCollider : MonoBehaviour
{

    // Use this for initialization   
    public float speed = 5;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnCollisionEnter2D(Collision2D Collider)
    {

        var ballRigid = Collider.gameObject.GetComponent<Rigidbody2D>();
        var dot = Vector2.Dot(ballRigid.velocity.normalized, Vector2.right);

        if (dot == 0)
        {
            return;
        }
        if (dot < 0)
            ballRigid.AddForce(Vector2.right * -speed * Time.deltaTime);
        else
            ballRigid.AddForce(Vector2.right * speed * Time.deltaTime);

    }

}
